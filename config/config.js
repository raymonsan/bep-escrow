const config = {
    CHAIN_WSURI:    process.env.CHAIN_WSURI,
    CHAIN_APIURI:   process.env.CHAIN_APIURI,
    CHAIN_START:    process.env.CHAIN_START,
    ESCROW_ADDRESS:  process.env.ESCROW_ADDRESS,
    ESCROW_PRIVKEY:  process.env.ESCROW_PRIVKEY,
    PAYER_ADDRESS:   process.env.PAYER_ADDRESS,
    PAYER_PRIVKEY:   process.env.PAYER_PRIVKEY,
    PAYEE_ADDRESS:  process.env.PAYEE_ADDRESS,
    PAYEE_PRIVKEY:  process.env.PAYEE_PRIVKEY,
    ADMIN_ADDRESS:  process.env.ADMIN_ADDRESS,
    ADMIN_PRIVKEY:  process.env.ADMIN_PRIVKEY,
    CHAIN_NET:      process.env.CHAIN_NET,
    EXPRESS_PORT:   process.env.PORT,
    BNBUSD_PAIR:    process.env.BNBUSD_PAIR,
    PRICE_APIURI:   process.env.PRICE_APIURI,
    CAN_TOKEN: process.env.CAN_TOKEN
}

exports.app = config;