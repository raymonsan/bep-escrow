jest.genMockFromModule('@binance-chain/javascript-sdk');
const hexEncoding = require('crypto-js/enc-hex')
const SHA256 = require('crypto-js/sha256')

const sha256 = (hex) => {
    if (typeof hex !== "string") throw new Error("sha256 expects a hex string")
    if (hex.length % 2 !== 0) throw new Error(`invalid hex string length: ${hex}`)
    const hexEncoded = hexEncoding.parse(hex)
    return SHA256(hexEncoded).toString()
  }

/* Random Hash */
const calculateRandomNumberHash = () => {
    const randomNumber = "e8eae926261ab77d018202434791a335249b470246a7b02e28c3b2fb6ffad8f3"
    const timestamp = Math.floor(Date.now()/1000)
    const timestampHexStr = timestamp.toString(16)
    let timestampHexStrFormat = timestampHexStr
    for (let i = 0; i < 16 - timestampHexStr.length; i++) {
      timestampHexStrFormat = '0' + timestampHexStrFormat;
    }
    const timestampBytes = Buffer.from(timestampHexStrFormat, "hex")
    const newBuffer = Buffer.concat([Buffer.from(randomNumber, "hex"), timestampBytes])
    return sha256(newBuffer.toString('hex'))
}

class BncClient {
    
    chooseNetwork(){
        return;
    }

    initChain(){
        return;
    }

    setPrivateKey(){
        return;
    }
    
    checkAddress(){
        return true;
    }

    transfer(){
        const result = {"result":[{"code":0,"hash":calculateRandomNumberHash(),"log":"Msg 0: ","ok":true}],"status":200}
        return result;
    }
}

module.exports = BncClient;
