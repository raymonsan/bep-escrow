const app = require('../config/config').app
const binance = require('../app/binance')
const escrow = require('../app/escrow')
const utils = require('../app/utils')
const fs = require('fs')

app.jobs = []
app.admins = []
app.state = true;
app.ESCROW_ADDRESS = 'BNBESCROW'

const testdata = JSON.parse(fs.readFileSync('./__mockData__/escrow.json'))
jest.setTimeout(10000);
const mockPrice = jest.spyOn(binance, "getPrice");
mockPrice.mockImplementation(async() =>{return 0.05});

const mockSyncing = jest.spyOn(utils, "syncing");
mockSyncing.mockImplementation(() =>{return false});

const mockLogging = jest.spyOn(utils, "logger");
mockLogging.mockImplementation(() =>{return true});

const mockCheckAddress = jest.spyOn(binance, "checkAddress");
mockCheckAddress.mockImplementation((address) =>{
  return address == 'BNBESCROW' ? false : true;
});

/* Setup jobs - Active Escrow*/
const newEscrow = async() => {
  await binance.binanceConnect()
  await escrow.handlePayload(testdata.inputs.escrow, null)
}

/* Setup job - Price Job */
const priceEscrow = async() => {
  await escrow.handlePayload(testdata.inputs.price,null)
}

/* Setup jobs - Released Escrow*/
const releasedEscrow = async() => {
  await escrow.handlePayload(testdata.inputs.release, null)
}

/* Setup jobs - Closed Escrow*/
const closedEscrow = async() => {
  await releasedEscrow();
  await escrow.handlePayload(testdata.inputs.disburse, null)
}



describe('SMOKE TESTS', () => {
  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  beforeEach(async() => {
    app.jobs = [];
    app.admins = [];
    app.state = true;
  })

  test('Escrow is created', async() => {
    await escrow.handlePayload(testdata.inputs.escrow, null)
    expect(app.jobs[0]).toMatchObject(testdata.outputs.escrow[0]);
  });

  test('Escrow is priced', async() => {
    await newEscrow()
    await escrow.handlePayload(testdata.inputs.price, null)
    expect(app.jobs[0]).toMatchObject(testdata.outputs.price[0]);
  })

  test('Escrow is released', async() =>{
    await newEscrow()
    await priceEscrow()
    await escrow.handlePayload(testdata.inputs.release, null)
    expect(app.jobs[0]).toMatchObject(testdata.outputs.released[0]);
  });

  test('Escrow is closed', async() =>{
    await newEscrow()
    await priceEscrow()
    await releasedEscrow()
    await escrow.handlePayload(testdata.inputs.disburse, null)
    expect(app.jobs[0]).toMatchObject(testdata.outputs.disbursed[0]);
    });

  test('Refunded tx is ignored', async() =>{
    let res = await escrow.handlePayload(testdata.inputs.refund, null)
    expect(res).toBe(true) 
  });
});

describe('DUPLICATE SCENARIOS ', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  test('Duplicate escrow fails', async() => {
    await newEscrow();
    let res = await escrow.handlePayload(testdata.inputs.escrow, null);
    expect(res.message).toBe('ESCROW_DUPLICATE')
  });

  test('Duplicate release fails', async() => {
    await newEscrow()
    await priceEscrow()
    await releasedEscrow()
    let res = await escrow.handlePayload(testdata.inputs.release, null);
    expect(res.message).toBe('RELEASE_DUPLICATE')
  });
});

describe('MALFORMED MEMOS', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  test('Empty memo fails', async() => {
    let res = await escrow.handlePayload(testdata.inputs.escrowEmptyMemo, null);
    expect(res.message).toBe('MEMO_EMPTY')
  });

  test('Release bad job fails', async() => {
    await newEscrow();
    let res = await escrow.handlePayload(testdata.inputs.badRelease, null);
    expect(res.message).toBe('RELEASE_NOJOB')
  });

  test('Escrow missing payee fails', async() => {
    let res = await escrow.handlePayload(testdata.inputs.escrowNoProvider, null);
    expect(res.message).toBe('MEMO_BAD')
  });

  test('Escrow with self fails', async() => {
    let res = await escrow.handlePayload(testdata.inputs.escrowSelf, null);
    expect(res.message).toBe('PROVIDER_BAD')
  });

  test('Escrow with escrow service fails', async() => {
    let res = await escrow.handlePayload(testdata.inputs.payerSelf, null);
    expect(res.message).toBe('PROVIDER_BAD')
  });
});


describe('FUNDING SCENARIOS', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  test('Fund command rejected if escrow is live', async() => {
    let res = await escrow.handlePayload(testdata.inputs.fund, null);
    expect(res.message).toBe('FUND_LATE')
  });

  test('Fund command accepted if no escrow', async() => { 
    app.admins = []
    app.jobs = []
    let res = await escrow.handlePayload(testdata.inputs.fund, null);
    expect(res.length).toBe(4)
  });
});

describe('REFUND SCENARIOS', () => {
  
  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  beforeEach(async() => {
    app.jobs = [];
    app.admins = [];
  })

  test('Admin refund request is processed', async() => { 
    app.admins = testdata.inputs.admins;
    await newEscrow()
    await priceEscrow()
    let res = await escrow.handlePayload(testdata.inputs.adminrefund, null);
    expect(res.status).toBe('REFUNDING')
  });

  test('Admin refund results in refund', async() => { 
    app.admins = testdata.inputs.admins;
    await newEscrow()
    await priceEscrow()
    await escrow.handlePayload(testdata.inputs.adminrefund, null);
    let res = await escrow.handlePayload(testdata.inputs.jobrefund, null);
    expect(res.status).toBe('REFUNDED')
  });

  test('Payee refund request is processed', async() => { 
    await newEscrow()
    await priceEscrow()
    let res = await escrow.handlePayload(testdata.inputs.payeerefund, null);
    expect(res.status).toBe('REFUNDING')
  });

  test('Client refund request is rejected', async() => { 
    await newEscrow()
    await priceEscrow()
    let res = await escrow.handlePayload(testdata.inputs.clientrefund, null);
    expect(res.message).toBe('REFUND_ERROR')
  });
});

describe('OTHER TESTS', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  test('transfer works', async() => {
    await releasedEscrow()
    let order = app.jobs[0];
    let res = await binance.transferToken(order,'RELEASE')
    expect(res).toBeDefined();
  });

  
});